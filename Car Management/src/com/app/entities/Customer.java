package com.app.entities;

import java.util.Comparator;
import java.util.HashMap;

public class Customer implements Comparator<Customer> {
	
	private int id;
	private String name;
	private HashMap<Integer,Car> cars;
	
	public Customer(int id,String name){
		this.name=name;
		this.id=id;
		cars=new HashMap<Integer,Car>();
	}
	
	public int getId() {
		return id;
	}

	public HashMap<Integer,Car> getCars() {
		return cars;
	}

	public String getName() {
		return name;
	}

	@Override
	public int compare(Customer c1, Customer c2) {
		int res = c1.getName().compareTo(c2.getName());
            return res != 0 ? res : 1;
	}

}
