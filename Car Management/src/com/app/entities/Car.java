package com.app.entities;

public class Car {
	
	protected int id;
	protected String model;
	protected long price;
	protected long resale_value;
	
	public int getId() {
		return id;
	}
	public String getModel() {
		return model;
	}
	public long getPrice() {
		return price;
	}
	public long getResale_value() {
		return resale_value;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public void setPrice(long l) {
		this.price = l;
	}
	public void setResale_value(long resale_value) {
		this.resale_value = resale_value;
	}

}
