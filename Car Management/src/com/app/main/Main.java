package com.app.main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import com.app.entities.Car;
import com.app.entities.Customer;
import com.app.entities.Hyundai;
import com.app.entities.Maruti;
import com.app.entities.Toyota;

public class Main {

	static Scanner sc=new Scanner(System.in);
	static Map<Integer,Customer> customers=new HashMap<Integer,Customer>();
	static int id;
	public static void addCustomer(){
		String name;
		do{
			try{
				System.out.println("Enter ID of Customer : ");
				id=sc.nextInt();
				if(customers.containsKey(id)){
					System.out.println("Customer ID already present !!!");
				}
				else{
					break;
				}
			   }
		catch(InputMismatchException e){
			System.out.println("Please Enter a Valid ID : ");
		}
		}while(true);
		System.out.println("Enter Name of new Customer : ");
		name=sc.next();
		customers.put(id, new Customer(id,name));
	}
	
	public static void addCar(){
		if(customers.size()==0){
			System.out.println("Sorry!! No Customers !!");
		}
		else{
		final int TOYOTA=1,MARUTI=2,HYUNDAI=3,EXIT=4;	
		do{
			try{
			System.out.println("Enter ID of Customer or Press E to Exit : ");
			String choice=sc.next();
			if(choice.equals("e") || choice.equals("E"))
				return ;
			else
			id=Integer.parseInt(choice);
			if(!customers.containsKey(id)){
				System.out.println("Customer ID not present !!!");
			}
			else
				break;
			}
			catch(InputMismatchException e){
				System.out.println("Please Enter a Valid ID : ");
			}
			}while(true);
		Customer customer=customers.get(id);
		int model_choice=0,flag=0;
		do{
			do{
			try{
				System.out.println("Choose model of car \n1.TOYOTA \n2.MARUTI \n3.HYUNDAI\nPress 4 to exit ");
				model_choice=sc.nextInt();
				flag=1;
			}
			catch(InputMismatchException e){
				System.out.println("Please Enter a Valid Choice : ");
			}
		}while(flag==0);
		Car car;
		switch(model_choice){
		
		case TOYOTA:
					car=new Toyota();
					createCar(customer,car,0.8);
					break;
					
		case MARUTI:
					car=new Maruti();
					createCar(customer,car,0.6);
					break;
					
		case HYUNDAI:
					car=new Hyundai();
					createCar(customer,car,0.4);
					break;
					
		case EXIT:
					
					break;
		
		default:
				System.out.println("Please Enter Correct choice");  
		}
		}while(model_choice!=EXIT);
	}
	}
	
	public static void createCar(Customer customer,Car car,double d){
		int cId;
		HashMap<Integer,Car> cars=customer.getCars();
		do{
			try{
			System.out.println("Enter ID of car : ");
			cId=sc.nextInt();
			if(cars.containsKey(cId)){
				System.out.println("Car ID already present !!!");}
			else
				break;
			}
			catch(InputMismatchException e){
				System.out.println("Please Enter a Valid ID : ");
			}
		}while(true);
		car.setId(cId);
		System.out.println("Enter model of car : ");
		car.setModel(sc.next());
		System.out.println("Enter price of car : ");
		car.setPrice(sc.nextLong());
		car.setResale_value((long) (car.getPrice()*d));
		customer.getCars().put(cId, car);
	}
	
	public static void displayCar(Car car,int i){
		System.out.println("CAR "+i);
		System.out.println("ID :"+car.getId());
		System.out.println("BRAND :"+car.getClass().getCanonicalName());
		System.out.println("MODEL :"+car.getModel());
		System.out.println("PRICE :"+car.getPrice());
		System.out.println("RESALE VALUE :"+car.getResale_value());
	}
	
	public static void display(Customer customer){
		System.out.println("CUSTOMER ID : "+ customer.getId());
		System.out.println("CUSTOMER NAME : "+customer.getName());
		System.out.println("CARS BELONGING TO CUSTOMER :");
		HashMap<Integer,Car> cars=customer.getCars();
		int i=1;
		List<Map.Entry<Integer,Car>> sortCar=new ArrayList<Map.Entry<Integer,Car>>(cars.entrySet());
		Collections.sort(sortCar,new Comparator<Map.Entry<Integer, Car>>(){
			@Override
			public int compare(Entry<Integer, Car> o1,Entry<Integer, Car> o2) {
				return o1.getValue().getModel().compareTo(o2.getValue().getModel());
			}
		});
			for (Map.Entry<Integer,Car> entry : sortCar) {
			displayCar(entry.getValue(),i++);
		}
	}
	
	public static void displayAll(){
		if(customers.size()==0){
			System.out.println("Sorry!! No Customers !!");
		}
		else{
		List<Map.Entry<Integer,Customer>> sortCustomer=new ArrayList<Map.Entry<Integer,Customer>>(customers.entrySet());
		Collections.sort(sortCustomer,new Comparator<Map.Entry<Integer, Customer>>(){
			@Override
			public int compare(Entry<Integer, Customer> o1,Entry<Integer, Customer> o2) {
				return o1.getValue().getName().compareTo(o2.getValue().getName());
			}
		});
		for (Map.Entry<Integer,Customer> entry : sortCustomer) {
			display(entry.getValue());
		}
	 }
	}

	public static void displayOne(){
		do{
			System.out.println("Enter ID of Customer or Press E to Exit : ");
			String choice=sc.next();
			if(choice.equals("e") || choice.equals("E"))
				return ;
			else
			id=Integer.parseInt(choice);
			if(!customers.containsKey(id)){
				System.out.println("Customer ID not present !!!");
			}
			}while(!customers.containsKey(id));
		display(customers.get(id));
	}
	
	public static void prize(){
		Random random=new Random();
		Set<Integer> rand = new HashSet<Integer>();
		Set<Integer> id = new HashSet<Integer>();
		if(customers.size()<6)
			System.out.println("Customers less than 6 !! Cannot generate a prize !!");
		else{
			while(rand.size()!=6){
			int r=random.nextInt(customers.size());
			rand.add(r);
			}
			
			System.out.println("Enter The 3 Customer IDs : ");
			for(int j=0;j<3;j++)
			id.add(sc.nextInt());
			
			System.out.println("Selected Customer IDs for Prize : ");
			Iterator<Integer> it = rand.iterator();
	        while (it.hasNext() ){
	            System.out.printf(" %d ",it.next());
	        }
	
			System.out.println("Entered Customer IDs for Prize : ");
			it = id.iterator();
	        while (it.hasNext() ){
	            System.out.printf(" %d ",it.next());
	        }
	        
			System.out.println("THe LUCKY WINNERS ARE :  ");
			rand.retainAll(id);
			it = rand.iterator();
	        while (it.hasNext() ){
	        	Customer customer=customers.get(it.next());
	        	System.out.println("CUSTOMER ID : "+ customer.getId());
	    		System.out.println("CUSTOMER NAME : "+customer.getName());
	        }
		}
	}
	
	public static void main(String[] args) {
		final int ADD_CUSTOMER=1,ADD_CAR=2,DISPLAY_ALL=3,DISPLAY_ONE=4,PRIZE=5,EXIT=6;
		
		int choice;
		do{
			System.out.println("1.Add Customer \n2.Add Car \n3.Display All \n4.Display Single Customer \n5.Prize \n6.Exit");
			choice=sc.nextInt();
			switch(choice){
			case ADD_CUSTOMER:
								addCustomer();
								break;
			
			case ADD_CAR:
							addCar();
							break;
				
			case DISPLAY_ALL:
								displayAll();
								break;
				
			case DISPLAY_ONE:
								displayOne();
								break;
				
			case PRIZE:
						prize();
						break;
				
			case EXIT:
						break;
						
			default:
				System.out.println("Please Enter Correct choice");
			}
		}while(choice!=EXIT);

	}
}
